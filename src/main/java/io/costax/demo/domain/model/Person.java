package io.costax.demo.domain.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "t_person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String name;

}
