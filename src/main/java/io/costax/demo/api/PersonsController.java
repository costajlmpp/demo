package io.costax.demo.api;

import io.costax.demo.domain.model.Person;
import io.costax.demo.domain.repository.PersonRepository;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(path = "/persons", produces = MediaType.APPLICATION_JSON_VALUE)
public class PersonsController {

    private final PersonRepository repository;

    public PersonsController(final PersonRepository repository) {
        this.repository = repository;
    }

    @GetMapping
    public List<Person> list() {
        return repository.findAll();
    }
}
