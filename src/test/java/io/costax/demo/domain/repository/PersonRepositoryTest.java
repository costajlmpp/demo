package io.costax.demo.domain.repository;

import io.costax.demo.domain.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

//@ActiveProfiles("its")
//@RunWith(SpringRunner.class)
//@org.springframework.test.context.TestPropertySource("classpath:database-test.properties") // The Spring active profile properties should be use
@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
//@Import(Populator.class)
class PersonRepositoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonRepositoryTest.class);

    @Autowired
    PersonRepository personRepository;

    @Test
    void getAll() {
        LOGGER.info("[UNIT] Running the UNIT tests");
        List<Person> all = personRepository.findAll();
        Assertions.assertNotNull(all);
        Assertions.assertEquals(2, all.size());
    }
}