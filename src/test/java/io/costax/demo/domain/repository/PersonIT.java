package io.costax.demo.domain.repository;

import io.costax.demo.domain.model.Person;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.ldap.AutoConfigureDataLdap;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest(showSql = false)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class PersonIT {

    private static final Logger LOGGER = LoggerFactory.getLogger(PersonIT.class);

    @Autowired
    PersonRepository personRepository;

    @Test
    void persons() {
        LOGGER.info("[IT] Running the IT tests");

        Person person = personRepository.findById(1).orElse(null);
        Assertions.assertNotNull(person);
    }
}
